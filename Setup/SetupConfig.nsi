; /*
; *********************************************************************************
; *                         Build.bat : BlackLight Project                        *
; *                                                                               *
; *  Date: 25 Sep 2011                                                            *
; *  Author: Mahdi Hamdarsi                                                       *
; *  Comments: Simple NSIS script to create Nvidia PhysX installers               *
; *                                                                               *
; *********************************************************************************
; */

; Global parameters
; =================================================================================

!include 'MUI2.nsh'
!include 'LogicLib.nsh'

!define Name "PhotoCompressor"
!define ExeName "${Name}.exe"
!define Company "Klaus"
!define Version "1.0"

Name "${Name} ${Version}"
OutFile "${Name} ${Version} Setup.exe"
InstallDir $PROGRAMFILES\${Name}
RequestExecutionLevel admin


; Declare used pages
; =================================================================================

!define MUI_ABORTWARNING
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_LANGUAGE "English"

; Declare all sections
; =================================================================================

; The main application
Section "${Name} Client"
  SectionIn RO

  ; Install application files
  SetOutPath $INSTDIR
  File "..\PhotoCompressor\bin\Release\${ExeName}"
  File "..\PhotoCompressor\bin\Release\FreeImage.dll"
  File "..\PhotoCompressor\bin\Release\FreeImageNet.dll"
  File "..\PhotoCompressor\bin\Release\ListViewPrinter.dll"
  File "..\PhotoCompressor\bin\Release\ObjectListView.dll"
  File "..\PhotoCompressor\bin\Release\Ookii.Dialogs.dll"
  File "..\PhotoCompressor\bin\Release\SparkleLibrary.dll"
  File "..\PhotoCompressor\bin\Release\System.Threading.Tasks.Dataflow.dll"

  ; Create shortcuts
  CreateShortCut "$SMPROGRAMS\${Name}.lnk" "$INSTDIR\${ExeName}"
  CreateShortCut "$DESKTOP\${Name}.lnk" "$INSTDIR\${ExeName}"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayName" "${Name} ${Version}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayVersion" "${Version}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayIcon" "$INSTDIR\${ExeName}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "Comments" "${Name}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "InstallLocation" "$INSTDIR"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "Publisher" "${Company}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
SectionEnd

; Uninstallation
Section "Uninstall"
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}"

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\${Name}.lnk"
  Delete "$DESKTOP\${Name}.lnk"

  ; Remove directories used
  RMDir /r "$INSTDIR"
SectionEnd
