﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading.Tasks.Dataflow;

using Microsoft.Win32;

using FreeImageAPI;
using FreeImageAPI.Metadata;

namespace PhotoCompressor
{
  public partial class ControlPanel : Form
  {
    #region Data
    private bool mExiting = false;
    private int mLandscapeHint = 2140;
    private int mPortraitHint = 2140;
    private string mSourceDir = "";
    private string mDestDir = "";

    private string mRoot;
    private string mOutputRoot;
    private List<Item> mSources = new List<Item>();

    private int mCounter = 0;
    private object mMutex = new object();
    private List<Item> mItemsToRefresh = new List<Item>();
    BufferBlock<Item> mDataFlow = new BufferBlock<Item>();
    #endregion Data


    #region Constructor
    public ControlPanel()
    {
      InitializeComponent();
      olvColumn2.AspectGetter = x => ((Item)x).State.ToString();
    }
    #endregion Constructor


    #region Parallel exections
    private void btnBrowseSource_Click(object sender, EventArgs e)
    {
      if(fdSource.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        return;

      mSources.Clear();
      int count = 0;
      ProcessFolder(mRoot = fdSource.SelectedPath, ref count);
      lvFiles.SetObjects(mSources);
      lblStatus.Text = "Selected " + count.ToString() + " files";
      btnStart.Enabled = mSources.Count > 0;

      mSourceDir = fdSource.SelectedPath;
      if (!mSourceDir.EndsWith("\\"))
        mSourceDir += "\\";
    }

    private void ProcessFolder(string input, ref int prevcount)
    {
      foreach (string file in Directory.GetFiles(input))
        mSources.Add(new Item(prevcount++, file.Remove(0, mRoot.Length + 1)));

      foreach (string dir in Directory.GetDirectories(input))
        ProcessFolder(dir, ref prevcount);
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      if(fdDest.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
        return;

      // Start updating completed items
      mDestDir = fdDest.SelectedPath;
      if (!mDestDir.EndsWith("\\"))
        mDestDir += "\\";

      mOutputRoot = fdDest.SelectedPath;
      Application.UseWaitCursor = true;
      lblStatus.Text = "Compressing...";
      btnStart.Enabled = false;
      btnBrowseSource.Enabled = false;
      tmProgress.Start();
      pbProgress.Visible = true;
      pbProgress.Minimum = 0;
      pbProgress.Maximum = mSources.Count;
      txtPortraitHint.Enabled = false;
      txtLandscapeHint.Enabled = false;
      mLandscapeHint = Convert.ToInt32(txtLandscapeHint.Text);
      mPortraitHint = Convert.ToInt32(txtPortraitHint.Text);
      mCounter = 0;

      // Send all items to be produced in a seperate thread
      Task.Run(() => {
        for (int i = 0; i < mSources.Count; ++i)
          mDataFlow.Post(mSources[i]);

        mDataFlow.Complete();
      });

      // Create threads to consume data. But just wait a little
      // to make sure that the first data is produced
      System.Threading.Thread.Sleep(100);
      for (int i = 0; i < Environment.ProcessorCount; ++i)
        new Thread(ProcessInThread).Start();
    }

    private void ProcessInThread(object data)
    {
      try
      {
        Item it;
        while ((it = mDataFlow.Receive()) != null && !mExiting)
        {
          // Queue to update to show item is being processed
          it.State = ItemState.Processing;
          lock (mMutex)
            { mItemsToRefresh.Add(it); }

          // Process the item
          try
          {
            string file = Path.Combine(mRoot, it.Path);
            string ext = Path.GetExtension(file).ToLower();
            string target = Path.Combine(mOutputRoot, it.Path);

            // Try to create output folder if not existing
            string dir = Path.GetDirectoryName(target);
            if (!Directory.Exists(dir))
              try
              { Directory.CreateDirectory(dir); }
              catch
              { }

            // Process the item
            if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
              ProcessPhoto(file, target);
            else
              File.Copy(file, target);

            it.State = ItemState.Done;
            Interlocked.Increment(ref mCounter);
          }
          catch (Exception e)
          {
            BeginInvoke(new Action(() =>
            {
              lblErrorMsg.Text = "Error: " + e.Message;
              Height = 457;
            }));

            it.State = ItemState.Error;
          }

          // Queue to update to show item is done
          lock (mMutex)
            { mItemsToRefresh.Add(it); }
        }
      }
      catch (InvalidOperationException)
      { }
    }

    private void tmProgress_Tick(object sender, EventArgs e)
    {
      // Check updated objects
      List<Item> items;
      lock(mMutex)
      {
        if(mItemsToRefresh.Count == 0)
          return;

        items = new List<Item>();
        items.AddRange(mItemsToRefresh);
        mItemsToRefresh.Clear();
      }

      // Refresh listview for updated objects
      int index = items[items.Count - 1].Index;
      lvFiles.RefreshObjects(items);
      lvFiles.EnsureVisible(index);
      pbProgress.Value = index;
      lblStatus.Text = String.Format("Processing... {0}/{1} ({2:F2}%)", index, mSources.Count, 100.0 * index / mSources.Count);

      // If the last item was processed, tidy up the UI
      if(mCounter == mSources.Count)
      { 
        Application.UseWaitCursor = false;
        lblStatus.Text = "All completed.";
        pbProgress.Visible = false;
        tmProgress.Stop();
      }
    }
    #endregion Parallel exections


    #region Item operations
    private void SuggestSize(ref int width, ref int height)
    {
      if (width > height)
      {
        // For landscape mode set height to 1200 and resize width accordingly
        width = mLandscapeHint * width / height;
        height = mLandscapeHint;
      }
      else
      {
        // For portrait pictures, set width to 1200 and resize height accordingly
        height = mPortraitHint * height / width;
        width = mPortraitHint;
      }
    }

    private FREE_IMAGE_FORMAT getInputFormat(string input)
    {
      input = input.ToLower();
      if(input.EndsWith("png"))
        return FREE_IMAGE_FORMAT.FIF_PNG;
      else if (input.EndsWith("jpg"))
        return FREE_IMAGE_FORMAT.FIF_JPEG;
      else if (input.EndsWith("bmp"))
        return FREE_IMAGE_FORMAT.FIF_BMP;
      else if (input.EndsWith("gif"))
        return FREE_IMAGE_FORMAT.FIF_GIF;
      else
        throw new Exception("Unsupported input format for: " + input);
    }

    private void ProcessPhoto(string input, string output)
    {
      FREE_IMAGE_FORMAT f = getInputFormat(input);
      FIBITMAP img = FreeImage.Load(f, input, 0);

      int width = (int)FreeImage.GetWidth(img);
      int height = (int)FreeImage.GetHeight(img);
      SuggestSize(ref width, ref height);
      FIBITMAP result = FreeImage.Rescale(img, (int)width, (int)height, FREE_IMAGE_FILTER.FILTER_BSPLINE);
      FreeImage.Save(FREE_IMAGE_FORMAT.FIF_JPEG, result, output, FREE_IMAGE_SAVE_FLAGS.JPEG_QUALITYGOOD);

      FreeImage.Unload(result);
      FreeImage.Unload(img);
    }
    #endregion Item operations


    #region Initialization and Finalization
    private void ControlPanel_FormClosing(object sender, FormClosingEventArgs e)
    {
      mExiting = true;
      RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32);
      key = key.CreateSubKey(@"Klaus\PhotoCompressor");
      key.SetValue("LandscapeHint", mLandscapeHint);
      key.SetValue("PortraitHint", mPortraitHint);
      key.SetValue("SourceDir", mSourceDir);
      key.SetValue("DestDir", mDestDir);
    }

    private void ControlPanel_Load(object sender, EventArgs e)
    {
      try
      { 
        Icon = Properties.Resources.ApplicationIcon;
        RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32);
        key = key.OpenSubKey(@"Klaus\PhotoCompressor");
        mLandscapeHint = Convert.ToInt32(key.GetValue("LandscapeHint"));
        mPortraitHint = Convert.ToInt32(key.GetValue("PortraitHint"));
        mSourceDir = key.GetValue("SourceDir").ToString();
        mDestDir = key.GetValue("DestDir").ToString();

        txtLandscapeHint.Text = mLandscapeHint.ToString();
        txtPortraitHint.Text = mPortraitHint.ToString();
        fdSource.SelectedPath = mSourceDir;
        fdDest.SelectedPath = mDestDir;
      }
      catch
      { }
    }
    #endregion Initialization and Finalization
  }
}
