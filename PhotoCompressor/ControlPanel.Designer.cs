﻿namespace PhotoCompressor
{
  partial class ControlPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.label1 = new System.Windows.Forms.Label();
      this.btnBrowseSource = new System.Windows.Forms.Button();
      this.fdSource = new Ookii.Dialogs.VistaFolderBrowserDialog();
      this.btnStart = new System.Windows.Forms.Button();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.pbProgress = new System.Windows.Forms.ToolStripProgressBar();
      this.tmProgress = new System.Windows.Forms.Timer(this.components);
      this.lvFiles = new BrightIdeasSoftware.FastObjectListView();
      this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
      this.fdDest = new Ookii.Dialogs.VistaFolderBrowserDialog();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.txtLandscapeHint = new System.Windows.Forms.TextBox();
      this.txtPortraitHint = new System.Windows.Forms.TextBox();
      this.lblErrorMsg = new System.Windows.Forms.Label();
      this.statusStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvFiles)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(82, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Files to convert:";
      // 
      // btnBrowseSource
      // 
      this.btnBrowseSource.Location = new System.Drawing.Point(340, 4);
      this.btnBrowseSource.Name = "btnBrowseSource";
      this.btnBrowseSource.Size = new System.Drawing.Size(91, 23);
      this.btnBrowseSource.TabIndex = 1;
      this.btnBrowseSource.Text = "Select Source";
      this.btnBrowseSource.UseVisualStyleBackColor = true;
      this.btnBrowseSource.Click += new System.EventHandler(this.btnBrowseSource_Click);
      // 
      // fdSource
      // 
      this.fdSource.Description = "Browse for the source folder containing your photos and media";
      this.fdSource.ShowNewFolderButton = false;
      this.fdSource.UseDescriptionForTitle = true;
      // 
      // btnStart
      // 
      this.btnStart.Enabled = false;
      this.btnStart.Location = new System.Drawing.Point(356, 337);
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new System.Drawing.Size(75, 23);
      this.btnStart.TabIndex = 3;
      this.btnStart.Text = "Start";
      this.btnStart.UseVisualStyleBackColor = true;
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.pbProgress});
      this.statusStrip1.Location = new System.Drawing.Point(0, 396);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(443, 22);
      this.statusStrip1.TabIndex = 5;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // lblStatus
      // 
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(128, 17);
      this.lblStatus.Text = "Select photos to begin.";
      // 
      // pbProgress
      // 
      this.pbProgress.Name = "pbProgress";
      this.pbProgress.Size = new System.Drawing.Size(200, 16);
      this.pbProgress.Visible = false;
      // 
      // tmProgress
      // 
      this.tmProgress.Tick += new System.EventHandler(this.tmProgress_Tick);
      // 
      // lvFiles
      // 
      this.lvFiles.AllColumns.Add(this.olvColumn1);
      this.lvFiles.AllColumns.Add(this.olvColumn2);
      this.lvFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2});
      this.lvFiles.FullRowSelect = true;
      this.lvFiles.Location = new System.Drawing.Point(12, 33);
      this.lvFiles.Name = "lvFiles";
      this.lvFiles.ShowGroups = false;
      this.lvFiles.Size = new System.Drawing.Size(419, 297);
      this.lvFiles.TabIndex = 6;
      this.lvFiles.UseCompatibleStateImageBehavior = false;
      this.lvFiles.View = System.Windows.Forms.View.Details;
      this.lvFiles.VirtualMode = true;
      // 
      // olvColumn1
      // 
      this.olvColumn1.AspectName = "Path";
      this.olvColumn1.CellPadding = null;
      this.olvColumn1.Text = "File";
      this.olvColumn1.Width = 312;
      // 
      // olvColumn2
      // 
      this.olvColumn2.CellPadding = null;
      this.olvColumn2.Text = "State";
      this.olvColumn2.Width = 83;
      // 
      // fdDest
      // 
      this.fdDest.Description = "Select the folder to save compressed photos";
      this.fdDest.UseDescriptionForTitle = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(9, 342);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(83, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Landscape hint:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(146, 342);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Portrait hint:";
      // 
      // txtLandscapeHint
      // 
      this.txtLandscapeHint.Location = new System.Drawing.Point(91, 339);
      this.txtLandscapeHint.Name = "txtLandscapeHint";
      this.txtLandscapeHint.Size = new System.Drawing.Size(38, 20);
      this.txtLandscapeHint.TabIndex = 7;
      this.txtLandscapeHint.Text = "2140";
      // 
      // txtPortraitHint
      // 
      this.txtPortraitHint.Location = new System.Drawing.Point(215, 339);
      this.txtPortraitHint.Name = "txtPortraitHint";
      this.txtPortraitHint.Size = new System.Drawing.Size(36, 20);
      this.txtPortraitHint.TabIndex = 7;
      this.txtPortraitHint.Text = "2140";
      // 
      // lblErrorMsg
      // 
      this.lblErrorMsg.AutoSize = true;
      this.lblErrorMsg.Location = new System.Drawing.Point(9, 368);
      this.lblErrorMsg.Name = "lblErrorMsg";
      this.lblErrorMsg.Size = new System.Drawing.Size(124, 13);
      this.lblErrorMsg.TabIndex = 8;
      this.lblErrorMsg.Text = "Error message goes here";
      // 
      // ControlPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(443, 418);
      this.Controls.Add(this.lblErrorMsg);
      this.Controls.Add(this.txtPortraitHint);
      this.Controls.Add(this.txtLandscapeHint);
      this.Controls.Add(this.lvFiles);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.btnStart);
      this.Controls.Add(this.btnBrowseSource);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "ControlPanel";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Compressor Utility";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControlPanel_FormClosing);
      this.Load += new System.EventHandler(this.ControlPanel_Load);
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.lvFiles)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnBrowseSource;
    private Ookii.Dialogs.VistaFolderBrowserDialog fdSource;
    private System.Windows.Forms.Button btnStart;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel lblStatus;
    private System.Windows.Forms.Timer tmProgress;
    private BrightIdeasSoftware.FastObjectListView lvFiles;
    private BrightIdeasSoftware.OLVColumn olvColumn1;
    private BrightIdeasSoftware.OLVColumn olvColumn2;
    private Ookii.Dialogs.VistaFolderBrowserDialog fdDest;
    private System.Windows.Forms.ToolStripProgressBar pbProgress;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtLandscapeHint;
    private System.Windows.Forms.TextBox txtPortraitHint;
    private System.Windows.Forms.Label lblErrorMsg;
  }
}