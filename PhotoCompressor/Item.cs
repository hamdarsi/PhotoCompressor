﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoCompressor
{
  public enum ItemState
  {
    Queued,
    Processing,
    Done,
    Error
  }


  public class Item
  {
    public int Index;
    public string Path = "";
    public ItemState State = ItemState.Queued;

    public Item(int index, string path)
    {
      Index = index;
      Path = path;
    }
  }
}
